float focus : register(c0);
sampler2D Input : register(s0);

float3 Mod(float3 x, float3 y);
float2 GetCoordinate(float2 uv);
float3 GetNormal(float3 normal);
float DistanceFunction(float3 pos);
float3 Trans(float3 p);
float3 Lighting(float3 p);

float3 Mod(float3 x, float3 y)
{
	return x - y * floor(x / y);
}

float2 GetCoordinate(float2 uv)
{
	return float2(2.0 * uv.x - 1.0, -1 * (2.0 * uv.y - 1.0));
}

float3 GetNormal(float3 x)
{
	const float d = 0.0001;
	const float f = DistanceFunction(x);
	return 
		normalize
		(
			float3
			(
				DistanceFunction(x + float3(d, 0.0, 0.0)) - f,
				DistanceFunction(x + float3(0.0, d, 0.0)) - f,
				DistanceFunction(x + float3(0.0, 0.0, d)) - f
			)
		);
}

float DistanceFunction(float3 pos)
{
	return length(Trans(pos)) - 1.0;
}

float3 Trans(float3 p)
{
	const float m = 4.0;
	return Mod(p, m) - m / 2.0;
}

float3 Lighting(float3 p)
{
	float3 n = GetNormal(p);
	float3 lightDir = normalize(float3(3, 3, 3) - p);
	float intensity = dot(n, lightDir);

	return float3(1, 1, 1) * intensity;
}

float4 main(float2 uv : TEXCOORD) : COLOR
{
	float2 pos = GetCoordinate(uv);

	float3 cameraPos = float3(0.0, 0.0, 3.0);
	float3 cameraDir = normalize(cameraPos)*-1.0;
	float3 cameraUp = float3(0.0, 1.0, 0.0);
	float3 cameraSide = cross(cameraDir, cameraUp);

	float3 rayDir = normalize(cameraSide*pos.x + cameraUp*pos.y + cameraDir*focus);

	float t = 0.0;
	float d = 0.0;
	float3 posOnRay = cameraPos;

	for (int i = 0; i < 64; ++i)
	{
		d = DistanceFunction(posOnRay);
		t += d;
		posOnRay = cameraPos + t*rayDir;
	}

	if (abs(d) < 0.001)
	{
		return float4(Lighting(posOnRay), 1);
	}
	else
	{
		return float4(0, 0, 0, 1);
	}
}
