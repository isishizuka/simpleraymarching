﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Media3D;

namespace SimpleRayMarching
{
	public class RayMarching : ShaderEffect
	{
		public static readonly DependencyProperty FocusProperty = DependencyProperty.Register("Focus", typeof(float), typeof(RayMarching), new PropertyMetadata(1.8f, PixelShaderConstantCallback(0)));
		public static readonly DependencyProperty InputProperty = ShaderEffect.RegisterPixelShaderSamplerProperty("Input", typeof(RayMarching), 0, SamplingMode.Auto);

		public RayMarching()
		{
			var pixelShader = new PixelShader();
			pixelShader.UriSource = new Uri("pack://application:,,,/Resource/RayMarching.ps", UriKind.Absolute);
			this.PixelShader = pixelShader;
			this.UpdateShaderValue(FocusProperty);
			this.UpdateShaderValue(InputProperty);
			this.DdxUvDdyUvRegisterIndex = -1;
		}

		public virtual float Focus
		{
			get
			{
				return (float)this.GetValue(FocusProperty);
			}
			set
			{
				this.SetValue(FocusProperty, value);
			}
		}
		public virtual Brush Input
		{
			get
			{
				return (Brush)this.GetValue(InputProperty);
			}
			set
			{
				this.SetValue(InputProperty, value);
			}
		}
	}
}