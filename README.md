This project will help you to learn how to use the ray marching.

# How to build
Open the SimpleRayMarching.sln, then run the project.

## Required software
DirectX SDK

Visual Studio 2013 or later.

## Required hardware
A graphic card supports shader model 3.0 or higher.

# Reference
I referred to this site [http://www.demoscene.jp/?p=811](http://www.demoscene.jp/?p=811).

Thanks a lot!

# License
All the source code in this project are under the [License.txt](License.txt).